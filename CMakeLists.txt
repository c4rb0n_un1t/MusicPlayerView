# Generated from MusicPlayerView.pro.

cmake_minimum_required(VERSION 3.16)
set( PROJECT_NAME MusicPlayerView )
project( ${PROJECT_NAME} )

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)

find_package(Qt6 COMPONENTS Core)
find_package(Qt6 COMPONENTS Gui)
find_package(Qt6 COMPONENTS QuickWidgets)
find_package(Qt6 COMPONENTS Qml)
find_package(Qt6 COMPONENTS Quick)

qt_add_plugin( ${PROJECT_NAME} SHARED )
set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "lib")
target_sources( ${PROJECT_NAME} PRIVATE
    ../../Interfaces/Architecture/GUIElementBase/../iguielement.h
    ../../Interfaces/Architecture/GUIElementBase/../referenceinstance.h
    ../../Interfaces/Architecture/GUIElementBase/../referenceinstanceslist.h
    ../../Interfaces/Architecture/GUIElementBase/../referenceshandler.h
    ../../Interfaces/Architecture/GUIElementBase/guielementbase.cpp ../../Interfaces/Architecture/GUIElementBase/guielementbase.h
    ../../Interfaces/Architecture/GUIElementBase/guielementlinkshandler.cpp ../../Interfaces/Architecture/GUIElementBase/guielementlinkshandler.h
    ../../Interfaces/Architecture/PluginBase/../iplugin.h
    ../../Interfaces/Architecture/PluginBase/../referenceinstance.h
    ../../Interfaces/Architecture/PluginBase/../referenceinstanceslist.h
    ../../Interfaces/Architecture/PluginBase/../referenceshandler.h
    ../../Interfaces/Architecture/PluginBase/plugin_base.cpp ../../Interfaces/Architecture/PluginBase/plugin_base.h
    ../../Interfaces/Architecture/PluginBase/plugindescriptor.h
    plugin.cpp plugin.h
    "res.qrc"
    "form.qml"
)
target_compile_definitions(MusicPlayerView PUBLIC
    QML_UIElement
)

target_link_libraries( ${PROJECT_NAME} PUBLIC
    Qt::Core
    Qt::Gui
    Qt::Qml
    Qt::Quick
    Qt::QuickWidgets
)

qt6_add_qml_module( MusicPlayerView_QML_StaticLib
    VERSION 1.0
    URI "Plugins"
)

set(SHARED_LIBRARY_NAME "${CMAKE_SHARED_LIBRARY_PREFIX}${PROJECT_NAME}${CMAKE_SHARED_LIBRARY_SUFFIX}")

add_custom_command(
        TARGET ${PROJECT_NAME} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy
                "${CMAKE_CURRENT_BINARY_DIR}/${SHARED_LIBRARY_NAME}"
                "${CMAKE_CURRENT_BINARY_DIR}/../../../Application/Plugins/${SHARED_LIBRARY_NAME}")
