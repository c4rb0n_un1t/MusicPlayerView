#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
	, m_GUIElementBase(new GUIElementBase(this, {"MainMenuItem"}, "qrc:/form.qml"))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this}
		, {INTERFACE(IGUIElement), m_GUIElementBase}
	},
	{
		{INTERFACE(IMusicPlayer), m_musicPlayer},
		{INTERFACE(IMusicPlayerTrackDataExtention), m_tracks}
	}
	);

	m_GUIElementBase->initGUIElementBase(
	{
		{"musicPlayer", m_musicPlayer.data()},
	}
	);
}

Plugin::~Plugin()
{
}

void Plugin::onReady()
{
	m_filter = m_tracks->getModel()->getFilter();
	m_filter->setColumns({
		{INTERFACE(IMusicPlayerTrackDataExtention), {"path"}}
	});
	m_GUIElementBase->rootContext()->setContextProperty("tracks", m_filter);
}
